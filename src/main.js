import Vue from 'vue';
import App from '@/App.vue';
import router from '@/routes/router';
import store from '@/store';
import '@/scss/_main.scss'
import '@/plugins';
import i18n from '@/plugins/i18n'
import '@/filters/filters';

Vue.config.productionTip = false;
Vue.config.devtools = true;

new Vue({
	router,
	store,
	i18n,
	created() {
		// if( store.getters.isAuthenticated ) {
		// 	router.push({ name: 'home' });
		// } else {
		// 	router.push({ name: 'signin' });
		// }
	},
	render: h => h(App)
}).$mount('#app');