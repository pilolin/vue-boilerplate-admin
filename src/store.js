import Vue from 'vue';
import Vuex from 'vuex';
import general from '@/store/general';
import auth from '@/store/auth';

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    general,
    auth,
  },
  state: {

  },
  mutations: {

  },
  actions: {

  }
})
