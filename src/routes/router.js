import Vue from 'vue';
import Router from 'vue-router';
import store from '@/store';

// middleware
import middlewarePipeline from './middlewarePipeline';
// import auth from '@/middleware/auth';
import guest from '@/middleware/guest';

import Signin from '@/views/Signin';
import Signup from '@/views/Signup';
import ForgotPassword from '@/views/ForgotPassword';
import Home from '@/views/Home';

Vue.use(Router);

const router = new Router ({
	routes: [
		{
			path: '',
			name: 'home',
			component: Home,
			meta: {
				// middleware: [ auth ]
			}
		},
		{
			path: '/signin',
			name: 'signin',
			component: Signin,
			meta: {
				middleware: [ guest ]
			}
		},
		{
			path: '/signup',
			name: 'signup',
			component: Signup,
			meta: {
				middleware: [ guest ]
			}
		},
		{
			path: '/forgot-password',
			name: 'forgotPassword',
			component: ForgotPassword,
			meta: {
				middleware: [ guest ]
			}
		}
	],
	mode: 'history'
});

router.beforeEach((to, from, next) => {
	if (!to.meta.middleware) {
		return next()
	}
	const middleware = to.meta.middleware;
	const context = { to, from, next, store };
	return middleware[0]({
		...context,
		next: middlewarePipeline(context, middleware, 1)
	});
})

export default router;