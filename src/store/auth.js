import {signup, signin} from '@/api/auth';
import axios from '@/plugins/axios';
import router from '@/routes/router';

export const SIGNUP = 'SIGNUP';
export const SIGNIN = 'SIGNIN';
export const SIGNOUT = 'SIGNOUT';

const state = {
	user: {
		accessToken: localStorage.getItem('access-token') || sessionStorage.getItem('access-token') || '',
		refreshToken: localStorage.getItem('refresh-token') || sessionStorage.getItem('refresh-token') || '',
		rememberMe: false
	}
};

const mutations = {
	SET_TOKENS(state, payload) {
		state.user = {
			accessToken: payload.accessToken,
			refreshToken: payload.refreshToken
		};
		if( state.user.rememberMe ) {
			localStorage.setItem('access-token', payload.accessToken ? payload.accessToken : '');
			localStorage.setItem('refresh-token', payload.refreshToken ? payload.refreshToken : '');
		} else {
			sessionStorage.setItem('access-token', payload.accessToken ? payload.accessToken : '');
			sessionStorage.setItem('refresh-token', payload.refreshToken ? payload.refreshToken : '');
		}

		axios.defaults.headers.common['Authorization'] = payload.accessToken ? 'Bearer ' + payload.accessToken : '';
	},
	UNSET_USER(state) {
		state.user = {
			accessToken: '',
			refreshToken: ''
		};
		if( state.user.rememberMe ) {
			localStorage.removeItem('access-token');
			localStorage.removeItem('refresh-token');
		} else {
			sessionStorage.removeItem('access-token');
			sessionStorage.removeItem('refresh-token');
		}
		axios.defaults.headers.common['Authorization'] = '';
	},
	REMEMBER_ME(state) {
		state.user.rememberMe = true;
	}
};

const actions = {
	[SIGNUP]: async ({ commit }, credentials) => {
		try {
			commit('SET_PROCESSING', true);

			const tokens = await signup(credentials);

			commit('SET_TOKENS', { ...tokens });

			commit('SET_PROCESSING', false);

			return tokens;
		} catch (error) {
			commit('SET_PROCESSING', false);

			throw error;
		}
	},
	[SIGNIN]: async ({ commit }, credentials) => {
		try {
			commit('SET_PROCESSING', true);

			const tokens = await signin(credentials);

			commit('SET_TOKENS', { ...tokens });

			commit('SET_PROCESSING', false);

			return tokens;
		} catch (error) {
			commit('SET_PROCESSING', false);

			throw error;
		}
	},
	[SIGNOUT]: async ({ commit }) => {
		commit('UNSET_USER');
		
		router.push({ name: 'signin' });
	}
};

const getters = {
	isAuthenticated: state => !!state.user.accessToken,
};

export default {
	state,
	mutations,
	actions,
	getters
}