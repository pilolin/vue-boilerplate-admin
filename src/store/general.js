const state = {
	pageLoaded: false,
};

const mutations = {
	SET_LOADED(state, loaded) {
		state.pageLoaded = loaded;
	}
};

const actions = {
	
};

const getters = {
	
};

export default {
	state,
	mutations,
	actions,
	getters
}