import '@/plugins/vue-js-modal';
import '@/plugins/vue-confirm';
import '@/plugins/vue-notification';
import '@/plugins/i18n';
import '@/plugins/base-components';