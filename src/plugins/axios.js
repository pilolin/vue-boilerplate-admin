import config from '@/config';
import axios from 'axios';
import humps from 'humps';
import store from '@/store';

const initAccessToken = localStorage.getItem('access-token') || sessionStorage.getItem('access-token') || ''

const instance = axios.create({
	baseURL: config.baseUrl,
  transformResponse: [
    ...axios.defaults.transformResponse,
    data => humps.camelizeKeys(data)
	],
	transformRequest: [
    data => humps.decamelizeKeys(data),
    ...axios.defaults.transformRequest
  ],
	headers: {
		common: {
			Authorization: initAccessToken ? 'Bearer ' +  initAccessToken : '',
			'Content-Type': 'application/json;charset=utf-8'
		}
	}
});

export default instance;

let isRefreshing = false;
let failedQueue = [];

const processQueue = (error, token = null) => {
	failedQueue.forEach(prom => {
		if (error) {
			prom.reject(error);
		} else {
			prom.resolve(token);
		}
	})

	failedQueue = [];
}

instance.interceptors.response.use(function (response) {
	return response;
}, function (error) {

	const originalRequest = error.config;
	const { response: { data } } = error;

	if (error.response.status === 401 && !originalRequest._retry) {

		if (isRefreshing) {
			return new Promise(function (resolve, reject) {
				failedQueue.push({
					resolve,
					reject
				})
			}).then(accessToken => {
				originalRequest.headers['Authorization'] = 'Bearer ' + accessToken;
				return axios(originalRequest);
			}).catch(err => {
				return err
			})
		}

		originalRequest._retry = true;
		isRefreshing = true;

		const refreshToken = localStorage.getItem('refresh-token') || sessionStorage.getItem('refresh-token');
		return new Promise(function (resolve, reject) {
			axios({
				method: 'post',
				url: `${instance.defaults.baseURL}/auth/refresh`,
				data: { refreshToken }
			})
			.then(({ data: { accessToken, refreshToken } }) => {
				store.commit('SET_TOKENS', {
					accessToken: accessToken,
					refreshToken: refreshToken
				});
				originalRequest.headers['Authorization'] = 'Bearer ' + accessToken;
				processQueue(null, accessToken);
				resolve(axios(originalRequest));
			})
			.catch((err) => {
				processQueue(err, null);
				reject(err);
				store.dispatch('SIGNOUT');
			})
			.then(() => {
				isRefreshing = false
			})
		})
	}

	if (error.response.status === 403 && error.config.url.indexOf('auth/login') !== -1) {
		data.message = 'You have been blocked. Contact your administrator for help.';
		data.errors = ['email', 'password'];
	}

	return Promise.reject(data);
});