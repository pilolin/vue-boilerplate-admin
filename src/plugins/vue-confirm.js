import Vue from 'vue';
import VueConfirmPlugin from 'vue-confirm-plugin';

Vue.use(VueConfirmPlugin);