import Vue from 'vue';

import { 
	BaseForm, 
	BaseFormGroup, 
	BaseButton, 
	BaseTextField, 
	BaseTextarea, 
	BaseCard, 
	BaseCheckbox, 
	BaseSwitch,
	BaseRadio, 
	BaseSearchField, 
	BaseInputImg,
	BaseSelect,
	BaseTable,
	BaseServerTable,
	BaseRoll,
	BaseDropdown,
	BaseModalMedia,
	BaseCardHeader, 
	BaseCardContent, 
	BaseCardFooter
} from '@/core/components';

Vue.component('bp-form', BaseForm);
Vue.component('bp-form-group', BaseFormGroup);
Vue.component('bp-button', BaseButton);
Vue.component('bp-text-field', BaseTextField);
Vue.component('bp-textarea', BaseTextarea);
Vue.component('bp-checkbox', BaseCheckbox);
Vue.component('bp-switch', BaseSwitch);
Vue.component('bp-radio', BaseRadio);
Vue.component('bp-search-field', BaseSearchField);
Vue.component('bp-input-img', BaseInputImg);
Vue.component('bp-select', BaseSelect);
Vue.component('bp-card', BaseCard);
Vue.component('bp-table', BaseTable);
Vue.component('bp-server-table', BaseServerTable);
Vue.component('bp-roll', BaseRoll);
Vue.component('bp-dropdown', BaseDropdown);
Vue.component('bp-modal-media', BaseModalMedia);

Vue.component('bp-card-header', BaseCardHeader);
Vue.component('bp-card-content', BaseCardContent);
Vue.component('bp-card-footer', BaseCardFooter);