import Vue from 'vue';
import VueI18n from 'vue-i18n';
import enUS from '@/lang/en-US';

Vue.use(VueI18n);

const locale = 'en-US';

const messages = {
	'en-US': enUS,
};

export default new VueI18n({
	locale,
	messages,
});