import Vue from 'vue';

Vue.filter('phone', phone => {
		const matches = phone.match(/(\d{1})(\d{3})(\d{3})(\d{4})/);
		return `+${matches[1]}(${matches[2]}) ${matches[3]}-${matches[4]}`;
});