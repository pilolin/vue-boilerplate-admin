export function getCookies() {
	const cookies = document.cookie.split('; ');
	let result = {};

	for (const cookie of cookies) {
		const arrCookie = cookie.split('=');
		result[arrCookie[0]] = arrCookie[1];
	}
	return result;
}

export function getCookieByName(name) {
	const cookies = document.cookie.split('; ');
	for (const cookie of cookies) {
		const arrCookie = cookie.split('=')
		if( arrCookie[0] === name )
			return arrCookie[1];
	}
}

export function setCookie(name, value, props = {}) {
	let cookie = `${name}=${value};`;
	for (const name in props) {
		cookie += `${name}=${props[name]};`;
	}
	document.cookie = cookie;
}

export function deleteCookie(name) {
	document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;domain=.venuepage.io;';
}