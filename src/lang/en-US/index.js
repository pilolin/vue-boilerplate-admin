import singleView from './singleView';
import other from './other';

export default {
	...singleView,
	other
}