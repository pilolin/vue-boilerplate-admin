export default {
	signin: {
		form: {
			title: `Welcome back.`,
			description: `Enter your details below.`,
			email: `Email`,
			password: `Password`,
			rememberMe: `Remember Me`,
			submit: `Log In`
		},
		signupLink: {
			text: `Don't have an account?`,
			link: `Sign Up`
		},
		recoveryLink: {
			text: `Forgot password from your account?`,
			link: `Recovery password`
		}
	},
	signup: {
		form: {
			title: `Welcome`,
			description: `Enter your details to create an account.`,
			name: `Name`,
			email: `Email`,
			password: `Password`,
			submit: `Create Account`
		},
		termsLink: {
			text: `By creating an account, you agree to our`,
			link: `Terms`
		},
		signinLink: {
			text: `Already have an account?`,
			link: `Sign In`
		}
	},
	forgotPassword: {
		form: {
			title: `Forgot password?`,
			description: `We'll send you an email with instructions to reset it.`,
			email: `Email`,
			submit: `Forgot Password`
		},
		signinLink: {
			text: `Already have an account?`,
			link: `Sign In`
		},
		notify: {
			success: {
				title: `Password reseted.`
			}
		}
	},
	home: {
	
	}
}