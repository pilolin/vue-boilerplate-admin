import axios from '@/plugins/axios';

export async function signup(credentials) {
	try {
		const { data } = await axios.post('/auth/register', {
			...credentials
		});

		return data;
	} catch(error) {
		throw error;
	}
}

export async function signin(credentials) {
	try {
		const { data } = await axios.post('/auth/login', {
			...credentials
		});

		return data;
	} catch(error) {
		throw error;
	}
}

export async function refresh(refreshToken) {
	try {
		const { data } = await axios.post('/auth/refresh', {
			refreshToken
		});

		return data;
	} catch(error) {
		throw error;
	}
}